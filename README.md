# custom-ingress

## Helm chart for custom ingress

This Helm chart configures `IngressController` resources plus a few additional resources:

* [ingress-publish-dns component](https://gitlab.cern.ch/paas-tools/okd4-deployment/ingress-publish-dns) for dynamic DNS of nodes and services
* LanDB alias (DelegatedDomain) for router canonical hostname
* wildcard certificate for router canonical hostname

The Helm chart is instantiated by ArgoCD multiples times (one chart deployment per ingress controller).

The full design documentation can be found in [okd-internal docs](https://okd-internal.docs.cern.ch/design/ingress-design/).

### Previous work

This essentially does the same as our [custom-haproxy-router](https://gitlab.cern.ch/paas-tools/custom-haproxy-router) did in Openshift 3, but for Openshift 4.

In addition, previously we were also building a custom HAProxy router image to set some environment variables (see [README.md @ ae527583](https://gitlab.cern.ch/paas-tools/okd4-deployment/custom-ingress/-/blob/ae527583afc03739f6235882d04d87f9b8f69176/README.md)).
This is no longer necessary since we now use a [customized cluster-ingress-operator](https://gitlab.cern.ch/paas-tools/okd4-deployment/cluster-ingress-operator) to set these environment variables (among other customizations).
